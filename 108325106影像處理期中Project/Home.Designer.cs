﻿namespace _108325106影像處理期中Project
{
    partial class Home
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置受控資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.RGB_To_Grey_Btn = new System.Windows.Forms.Button();
            this.channel_conver = new System.Windows.Forms.Button();
            this.Binarization = new System.Windows.Forms.Button();
            this.Erode_Dilate = new System.Windows.Forms.Button();
            this.Denoise = new System.Windows.Forms.Button();
            this.Sharpen = new System.Windows.Forms.Button();
            this.Sobel = new System.Windows.Forms.Button();
            this.Histogram = new System.Windows.Forms.Button();
            this.Rotation = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // RGB_To_Grey_Btn
            // 
            this.RGB_To_Grey_Btn.Location = new System.Drawing.Point(24, 25);
            this.RGB_To_Grey_Btn.Name = "RGB_To_Grey_Btn";
            this.RGB_To_Grey_Btn.Size = new System.Drawing.Size(75, 23);
            this.RGB_To_Grey_Btn.TabIndex = 0;
            this.RGB_To_Grey_Btn.Text = "灰階轉換";
            this.RGB_To_Grey_Btn.UseVisualStyleBackColor = true;
            this.RGB_To_Grey_Btn.Click += new System.EventHandler(this.RGB_To_Grey_Btn_Click);
            // 
            // channel_conver
            // 
            this.channel_conver.Location = new System.Drawing.Point(24, 83);
            this.channel_conver.Name = "channel_conver";
            this.channel_conver.Size = new System.Drawing.Size(75, 23);
            this.channel_conver.TabIndex = 1;
            this.channel_conver.Text = "通道轉換";
            this.channel_conver.UseVisualStyleBackColor = true;
            this.channel_conver.Click += new System.EventHandler(this.channel_conver_Click);
            // 
            // Binarization
            // 
            this.Binarization.Location = new System.Drawing.Point(24, 143);
            this.Binarization.Name = "Binarization";
            this.Binarization.Size = new System.Drawing.Size(75, 23);
            this.Binarization.TabIndex = 2;
            this.Binarization.Text = "二值化";
            this.Binarization.UseVisualStyleBackColor = true;
            this.Binarization.Click += new System.EventHandler(this.Binarization_Click);
            // 
            // Erode_Dilate
            // 
            this.Erode_Dilate.Location = new System.Drawing.Point(24, 205);
            this.Erode_Dilate.Name = "Erode_Dilate";
            this.Erode_Dilate.Size = new System.Drawing.Size(75, 23);
            this.Erode_Dilate.TabIndex = 3;
            this.Erode_Dilate.Text = "侵蝕和擴張";
            this.Erode_Dilate.UseVisualStyleBackColor = true;
            this.Erode_Dilate.Click += new System.EventHandler(this.Erode_Dilate_Click);
            // 
            // Denoise
            // 
            this.Denoise.Location = new System.Drawing.Point(151, 25);
            this.Denoise.Name = "Denoise";
            this.Denoise.Size = new System.Drawing.Size(75, 23);
            this.Denoise.TabIndex = 4;
            this.Denoise.Text = "去雜訊";
            this.Denoise.UseVisualStyleBackColor = true;
            this.Denoise.Click += new System.EventHandler(this.Denoise_Click);
            // 
            // Sharpen
            // 
            this.Sharpen.Location = new System.Drawing.Point(151, 83);
            this.Sharpen.Name = "Sharpen";
            this.Sharpen.Size = new System.Drawing.Size(75, 23);
            this.Sharpen.TabIndex = 5;
            this.Sharpen.Text = "銳化";
            this.Sharpen.UseVisualStyleBackColor = true;
            this.Sharpen.Click += new System.EventHandler(this.Sharpen_Click);
            // 
            // Sobel
            // 
            this.Sobel.Location = new System.Drawing.Point(151, 143);
            this.Sobel.Name = "Sobel";
            this.Sobel.Size = new System.Drawing.Size(75, 23);
            this.Sobel.TabIndex = 6;
            this.Sobel.Text = "邊緣檢測";
            this.Sobel.UseVisualStyleBackColor = true;
            this.Sobel.Click += new System.EventHandler(this.Sobel_Click);
            // 
            // Histogram
            // 
            this.Histogram.Location = new System.Drawing.Point(151, 205);
            this.Histogram.Name = "Histogram";
            this.Histogram.Size = new System.Drawing.Size(75, 23);
            this.Histogram.TabIndex = 7;
            this.Histogram.Text = "直方圖等化";
            this.Histogram.UseVisualStyleBackColor = true;
            this.Histogram.Click += new System.EventHandler(this.Histogram_Click);
            // 
            // Rotation
            // 
            this.Rotation.Location = new System.Drawing.Point(87, 264);
            this.Rotation.Name = "Rotation";
            this.Rotation.Size = new System.Drawing.Size(75, 23);
            this.Rotation.TabIndex = 8;
            this.Rotation.Text = "旋轉";
            this.Rotation.UseVisualStyleBackColor = true;
            this.Rotation.Click += new System.EventHandler(this.Rotation_Click);
            // 
            // Home
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(261, 309);
            this.Controls.Add(this.Rotation);
            this.Controls.Add(this.Histogram);
            this.Controls.Add(this.Sobel);
            this.Controls.Add(this.Sharpen);
            this.Controls.Add(this.Denoise);
            this.Controls.Add(this.Erode_Dilate);
            this.Controls.Add(this.Binarization);
            this.Controls.Add(this.channel_conver);
            this.Controls.Add(this.RGB_To_Grey_Btn);
            this.Name = "Home";
            this.Text = "Home";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button RGB_To_Grey_Btn;
        private System.Windows.Forms.Button channel_conver;
        private System.Windows.Forms.Button Binarization;
        private System.Windows.Forms.Button Erode_Dilate;
        private System.Windows.Forms.Button Denoise;
        private System.Windows.Forms.Button Sharpen;
        private System.Windows.Forms.Button Sobel;
        private System.Windows.Forms.Button Histogram;
        private System.Windows.Forms.Button Rotation;
    }
}

