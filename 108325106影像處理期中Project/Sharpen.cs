﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _108325106影像處理期中Project
{
    public partial class Sharpen : Form
    {
        public Sharpen()
        {
            InitializeComponent();
        }

        private void openFile_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog dlg = new OpenFileDialog())
            {
                dlg.Title = "Open Image";
                dlg.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png";

                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    originalPicture.Image = new Bitmap(dlg.FileName);
                    originalPicture.SizeMode = PictureBoxSizeMode.StretchImage;
                }
            }
        }

        private void convert_Click(object sender, EventArgs e)
        {
            if (originalPicture.Image != null)
            {
                Bitmap _originalPicture = new Bitmap(originalPicture.Image);

                newPicture.Image = _Sharpen(_originalPicture);
                newPicture.SizeMode = PictureBoxSizeMode.StretchImage;
            }
        }

        public Bitmap _Sharpen(Bitmap original)
        {
            Bitmap newBitmap = new Bitmap(original.Width, original.Height);
            int[] Laplacian = { -1, -1, -1, -1, 9, -1, -1, -1, -1 };

            for (int x = 1; x < original.Width - 1; x++)
            {
                for (int y = 1; y < original.Height - 1; y++)
                {
                    int r = 0, g = 0, b = 0;
                    int Index = 0;

                    for (int i = -1; i < 2; i++)
                    {
                        for (int j = -1; j < 2; j++)
                        {
                            Color pixel = original.GetPixel(x + i, y + j);

                            r += pixel.R * Laplacian[Index];
                            g += pixel.G * Laplacian[Index];
                            b += pixel.B * Laplacian[Index];

                            Index++;
                        }
                    }

                    r = r > 255 ? 255 : (r < 0 ? 0 : r);
                    g = g > 255 ? 255 : (g < 0 ? 0 : g);
                    b = b > 255 ? 255 : (b < 0 ? 0 : b);

                    newBitmap.SetPixel(x, y, Color.FromArgb(r, g, b));
                }
            }

            return newBitmap;
        }
    }
}
