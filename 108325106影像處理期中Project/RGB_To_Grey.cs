﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _108325106影像處理期中Project
{
    public partial class RGB_To_Grey : Form
    {
        public RGB_To_Grey()
        {
            InitializeComponent();
        }

        private void openFile_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog dlg = new OpenFileDialog())
            {
                dlg.Title = "Open Image";  
                dlg.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png";  

                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    originalPicture.Image = new Bitmap(dlg.FileName);
                    originalPicture.SizeMode = PictureBoxSizeMode.StretchImage;
                }
            }
        }

        private void convert_Click(object sender, EventArgs e)
        {
            if(originalPicture.Image != null)
            {
                Bitmap _originalPicture = new Bitmap(originalPicture.Image);

                newPicture.Image = MakeGreyscale(_originalPicture);
                newPicture.SizeMode = PictureBoxSizeMode.StretchImage;
            }
        }

        public Bitmap MakeGreyscale(Bitmap original)
        {
            Bitmap newBitmap = new Bitmap(original.Width, original.Height);

            for (int x = 0; x < newBitmap.Width; x++)
            {
                for (int y = 0; y < newBitmap.Height; y++)
                {
                    Color originalColor = original.GetPixel(x, y);  //取得原始圖片的像素
                    int greyScale = (int)((originalColor.R * .3) + (originalColor.G * .59) + (originalColor.B * .11));   //將每一像素RGB to Grey
                    Color newColor = Color.FromArgb(greyScale, greyScale, greyScale);   //重構顏色
                    newBitmap.SetPixel(x, y, newColor); //將轉換後的灰階存回新的圖片
                }
            }

            return newBitmap;
        }

    }
}
