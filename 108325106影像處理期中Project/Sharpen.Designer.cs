﻿namespace _108325106影像處理期中Project
{
    partial class Sharpen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.convert = new System.Windows.Forms.Button();
            this.newPicture = new System.Windows.Forms.PictureBox();
            this.openFile = new System.Windows.Forms.Button();
            this.originalPicture = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.newPicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.originalPicture)).BeginInit();
            this.SuspendLayout();
            // 
            // convert
            // 
            this.convert.Location = new System.Drawing.Point(359, 207);
            this.convert.Name = "convert";
            this.convert.Size = new System.Drawing.Size(75, 23);
            this.convert.TabIndex = 11;
            this.convert.Text = "銳化";
            this.convert.UseVisualStyleBackColor = true;
            this.convert.Click += new System.EventHandler(this.convert_Click);
            // 
            // newPicture
            // 
            this.newPicture.Location = new System.Drawing.Point(548, 111);
            this.newPicture.Name = "newPicture";
            this.newPicture.Size = new System.Drawing.Size(240, 210);
            this.newPicture.TabIndex = 10;
            this.newPicture.TabStop = false;
            // 
            // openFile
            // 
            this.openFile.Location = new System.Drawing.Point(12, 12);
            this.openFile.Name = "openFile";
            this.openFile.Size = new System.Drawing.Size(75, 23);
            this.openFile.TabIndex = 9;
            this.openFile.Text = "選擇檔案";
            this.openFile.UseVisualStyleBackColor = true;
            this.openFile.Click += new System.EventHandler(this.openFile_Click);
            // 
            // originalPicture
            // 
            this.originalPicture.Location = new System.Drawing.Point(12, 111);
            this.originalPicture.Name = "originalPicture";
            this.originalPicture.Size = new System.Drawing.Size(240, 210);
            this.originalPicture.TabIndex = 8;
            this.originalPicture.TabStop = false;
            // 
            // Sharpen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.convert);
            this.Controls.Add(this.newPicture);
            this.Controls.Add(this.openFile);
            this.Controls.Add(this.originalPicture);
            this.Name = "Sharpen";
            this.Text = "Sharpen";
            ((System.ComponentModel.ISupportInitialize)(this.newPicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.originalPicture)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button convert;
        private System.Windows.Forms.PictureBox newPicture;
        private System.Windows.Forms.Button openFile;
        private System.Windows.Forms.PictureBox originalPicture;
    }
}