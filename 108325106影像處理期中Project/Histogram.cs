﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _108325106影像處理期中Project
{
    public partial class Histogram : Form
    {
        public Histogram()
        {
            InitializeComponent();
        }

        public class RGBCount
        {
            public int[] R { get; set; }
            public int[] G { get; set; }
            public int[] B { get; set; }
        }

        private void openFile_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog dlg = new OpenFileDialog())
            {
                dlg.Title = "Open Image";
                dlg.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png";

                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    originalPicture.Image = new Bitmap(dlg.FileName);
                    originalPicture.SizeMode = PictureBoxSizeMode.StretchImage;
                }
            }
        }

        private void convert_Click(object sender, EventArgs e)
        {
            if (originalPicture.Image != null)
            {
                Bitmap _originalPicture = new Bitmap(originalPicture.Image);

                newPicture.Image = MakeHistogram(_originalPicture);
                newPicture.SizeMode = PictureBoxSizeMode.StretchImage;
            }
        }

        public Bitmap MakeHistogram(Bitmap original)
        {
            Bitmap newBitmap = new Bitmap(original.Width, original.Height);

            RGBCount pdf = MakePDF(original);
            RGBCount cdf = MakeCDF(pdf);

            for (int x = 0; x < original.Width; x++)
            {
                for (int y = 0; y < original.Height; y++)
                {
                    int r = MathHistogram(cdf.R, original.GetPixel(x, y).R, original.Width * original.Height);
                    int g = MathHistogram(cdf.G, original.GetPixel(x, y).G, original.Width * original.Height);
                    int b = MathHistogram(cdf.B, original.GetPixel(x, y).B, original.Width * original.Height);

                    newBitmap.SetPixel(x, y, Color.FromArgb(r, g, b));
                }
            }

            return newBitmap;
        }

        public RGBCount MakePDF(Bitmap original)
        {
            RGBCount pdf = new RGBCount();
            pdf.R = new int[256];
            pdf.G = new int[256];
            pdf.B = new int[256];

            for (int x = 0; x < original.Width; x++)
            {
                for (int y = 0; y < original.Height; y++)
                {
                    int r = original.GetPixel(x, y).R;
                    pdf.R[r]++;

                    int g = original.GetPixel(x, y).G;
                    pdf.G[g]++;

                    int b = original.GetPixel(x, y).B;
                    pdf.B[b]++;
                }
            }

            return pdf;
        }

        public RGBCount MakeCDF(RGBCount pdf)
        {
            RGBCount cdf = new RGBCount();
            cdf.R = new int[256];
            cdf.G = new int[256];
            cdf.B = new int[256];

            cdf.R[0] = pdf.R[0];
            for (int i = 1; i < 256; i++)
            {
                cdf.R[i] = cdf.R[i - 1] + pdf.R[i];
            }

            cdf.G[0] = pdf.G[0];
            for (int i = 1; i < 256; i++)
            {
                cdf.G[i] = cdf.G[i - 1] + pdf.G[i];
            }

            cdf.B[0] = pdf.B[0];
            for (int i = 1; i < 256; i++)
            {
                cdf.B[i] = cdf.B[i - 1] + pdf.B[i];
            }

            return cdf;
        }

        public int MathHistogram(int[] cdf, int pixel, int MN)
        {
            double H = ((double)(cdf[pixel] - cdf[0]) / MN) * 255;
            H = Math.Round(H);

            return (int)H;
        }
    }
}
