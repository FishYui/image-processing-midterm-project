﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _108325106影像處理期中Project
{
    public partial class Denoise : Form
    {
        public Denoise()
        {
            InitializeComponent();
        }

        private void openFile_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog dlg = new OpenFileDialog())
            {
                dlg.Title = "Open Image";
                dlg.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png";

                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    originalPicture.Image = new Bitmap(dlg.FileName);
                    originalPicture.SizeMode = PictureBoxSizeMode.StretchImage;
                }
            }
        }

        private void convert_Click(object sender, EventArgs e)
        {
            if (originalPicture.Image != null)
            {
                Bitmap _originalPicture = new Bitmap(originalPicture.Image);

                newPicture.Image = _Denoise(_originalPicture);
                newPicture.SizeMode = PictureBoxSizeMode.StretchImage;
            }
        }

        public Bitmap _Denoise(Bitmap original)
        {
            Bitmap newBitmap = new Bitmap(original.Width, original.Height);

            for (int x = 0; x < original.Width; x++)
            {
                for (int y = 0; y < original.Height; y++)
                {
                    newBitmap.SetPixel(x, y, GetMidColor(original, x, y));
                }
            }

            return newBitmap;
        }

        public Color GetMidColor(Bitmap original, int X, int Y)
        {
            List<int> color = new List<int>();

            for (int i = -1; i < 2; i++)
            {
                for (int j = -1; j < 2; j++)
                {
                    if (X + i < 0 || X + i >= original.Width || Y + j < 0 || Y + j >= original.Height)  //防止index超出相片範圍
                        continue;

                    color.Add(original.GetPixel(X + i, Y + j).R);  //取得遮罩範圍內的像素顏色
                }
            }

            color.Sort();

            int count = color.Count();
            int mid = count % 2 == 0 ? (color[(count / 2) - 1] + color[(count / 2)]) / 2 : color[count / 2];

            return Color.FromArgb(mid, mid, mid);
        }
    }
}
