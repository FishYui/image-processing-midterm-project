﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _108325106影像處理期中Project
{
    public partial class Rotation : Form
    {
        public Rotation()
        {
            InitializeComponent();
        }

        private void openFile_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog dlg = new OpenFileDialog())
            {
                dlg.Title = "Open Image";
                dlg.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png";

                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    originalPicture.Image = new Bitmap(dlg.FileName);
                    originalPicture.SizeMode = PictureBoxSizeMode.StretchImage;
                }
            }
        }

        private void trackBar1_ValueChanged(object sender, EventArgs e)
        {
            int angle = trackBar1.Value;
            RotationText.Text = angle.ToString();

            if (originalPicture.Image != null)
            {
                Bitmap original = new Bitmap(originalPicture.Image);

                if (angle < 0)
                    angle += 360;

                newPicture.Image = BitmapRotation(original, angle);
                newPicture.SizeMode = PictureBoxSizeMode.StretchImage;
            }
        }

        private void RotaationText_TextChanged(object sender, EventArgs e)
        {
            try
            {
                int angle = Convert.ToInt32(RotationText.Text);
                angle = angle > 360 ? (angle % 360) : (angle < -360 ? (angle % -360) : angle);

                trackBar1.Value = angle;
            }
            catch
            {
                return;
            }
        }

        public Bitmap BitmapRotation(Bitmap original, int angle)
        {
            Bitmap newBitmap = new Bitmap(original.Width, original.Height);

            Color black = Color.FromArgb(0, 0, 0);

            double sin = Math.Sin(angle * (float)(Math.PI / 180));
            double cos = Math.Cos(angle * (float)(Math.PI / 180));

            int originalX = original.Width / 2;
            int originalY = original.Height / 2;

            int _originalX = (int)(originalY * sin + originalX * cos);
            int _originalY = (int)(originalY * cos - originalX * sin);

            int deviationX = Math.Abs(originalX - _originalX);
            int deviationY = Math.Abs(originalY - _originalY);

            deviationX = angle < 90 ? -deviationX : deviationX;
            deviationY = angle > 270 ? -deviationY : deviationY;

            for (int x = 0; x < newBitmap.Width; x++)
            {
                for (int y = 0; y < newBitmap.Height; y++)
                {
                    int _x = (int)(y * sin + x * cos) + deviationX;
                    int _y = (int)(y * cos - x * sin) + deviationY;

                    if (_x < 0 || _x >= original.Width || _y < 0 || _y >= original.Height)
                        newBitmap.SetPixel(x, y, black);
                    else
                        newBitmap.SetPixel(x, y, original.GetPixel(_x, _y));
                }
            }

            return newBitmap;
        }
    }
}
