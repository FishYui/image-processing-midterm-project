﻿namespace _108325106影像處理期中Project
{
    partial class Erode_Dilate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.openFile = new System.Windows.Forms.Button();
            this.erode_btn = new System.Windows.Forms.Button();
            this.originalPicture = new System.Windows.Forms.PictureBox();
            this.erodePicture = new System.Windows.Forms.PictureBox();
            this.dilatePicture = new System.Windows.Forms.PictureBox();
            this.dilate_btn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.originalPicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.erodePicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dilatePicture)).BeginInit();
            this.SuspendLayout();
            // 
            // openFile
            // 
            this.openFile.Location = new System.Drawing.Point(12, 12);
            this.openFile.Name = "openFile";
            this.openFile.Size = new System.Drawing.Size(75, 23);
            this.openFile.TabIndex = 2;
            this.openFile.Text = "選擇檔案";
            this.openFile.UseVisualStyleBackColor = true;
            this.openFile.Click += new System.EventHandler(this.openFile_Click);
            // 
            // erode_btn
            // 
            this.erode_btn.Location = new System.Drawing.Point(330, 216);
            this.erode_btn.Name = "erode_btn";
            this.erode_btn.Size = new System.Drawing.Size(75, 23);
            this.erode_btn.TabIndex = 6;
            this.erode_btn.Text = "侵蝕";
            this.erode_btn.UseVisualStyleBackColor = true;
            this.erode_btn.Click += new System.EventHandler(this.erode_btn_Click);
            // 
            // originalPicture
            // 
            this.originalPicture.Location = new System.Drawing.Point(491, 120);
            this.originalPicture.Name = "originalPicture";
            this.originalPicture.Size = new System.Drawing.Size(240, 210);
            this.originalPicture.TabIndex = 5;
            this.originalPicture.TabStop = false;
            // 
            // erodePicture
            // 
            this.erodePicture.Location = new System.Drawing.Point(12, 120);
            this.erodePicture.Name = "erodePicture";
            this.erodePicture.Size = new System.Drawing.Size(240, 210);
            this.erodePicture.TabIndex = 4;
            this.erodePicture.TabStop = false;
            // 
            // dilatePicture
            // 
            this.dilatePicture.Location = new System.Drawing.Point(946, 120);
            this.dilatePicture.Name = "dilatePicture";
            this.dilatePicture.Size = new System.Drawing.Size(240, 210);
            this.dilatePicture.TabIndex = 7;
            this.dilatePicture.TabStop = false;
            // 
            // dilate_btn
            // 
            this.dilate_btn.Location = new System.Drawing.Point(802, 216);
            this.dilate_btn.Name = "dilate_btn";
            this.dilate_btn.Size = new System.Drawing.Size(75, 23);
            this.dilate_btn.TabIndex = 8;
            this.dilate_btn.Text = "擴張";
            this.dilate_btn.UseVisualStyleBackColor = true;
            this.dilate_btn.Click += new System.EventHandler(this.dilate_btn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(111, 333);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 12);
            this.label1.TabIndex = 9;
            this.label1.Text = "Erode";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(1054, 333);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 12);
            this.label2.TabIndex = 10;
            this.label2.Text = "Dilate";
            // 
            // Erode_Dilate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1198, 450);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dilate_btn);
            this.Controls.Add(this.dilatePicture);
            this.Controls.Add(this.erode_btn);
            this.Controls.Add(this.originalPicture);
            this.Controls.Add(this.erodePicture);
            this.Controls.Add(this.openFile);
            this.Name = "Erode_Dilate";
            this.Text = "Erode_Dilate";
            ((System.ComponentModel.ISupportInitialize)(this.originalPicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.erodePicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dilatePicture)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button openFile;
        private System.Windows.Forms.Button erode_btn;
        private System.Windows.Forms.PictureBox originalPicture;
        private System.Windows.Forms.PictureBox erodePicture;
        private System.Windows.Forms.PictureBox dilatePicture;
        private System.Windows.Forms.Button dilate_btn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}