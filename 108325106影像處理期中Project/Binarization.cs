﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _108325106影像處理期中Project
{
    public partial class Binarization : Form
    {
        public Binarization()
        {
            InitializeComponent();
        }

        private void openFile_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog dlg = new OpenFileDialog())
            {
                dlg.Title = "Open Image";
                dlg.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png";

                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    originalPicture.Image = new Bitmap(dlg.FileName);
                    originalPicture.SizeMode = PictureBoxSizeMode.StretchImage;
                }
            }
        }

        private void trackBar1_ValueChanged(object sender, EventArgs e)
        {
            GrayscaleText.Text = (trackBar1.Value).ToString();

            if (originalPicture.Image != null)
            {
                Bitmap original = new Bitmap(originalPicture.Image);

                newPicture.Image = Binarize(original, trackBar1.Value);
                newPicture.SizeMode = PictureBoxSizeMode.StretchImage;
            }
        }

        private void GrayscaleText_TextChanged(object sender, EventArgs e)
        {
            try
            {
                int grayscale = Convert.ToInt32(GrayscaleText.Text);

                if (grayscale > 255)
                {
                    grayscale = 255;
                }
                else if (grayscale < 0)
                {
                    grayscale = 0;
                }

                trackBar1.Value = grayscale;
            }
            catch
            {
                return;
            }
        }

        public Bitmap Binarize(Bitmap original, int grayscale)
        {
            Bitmap newBitmap = new Bitmap(original.Width, original.Height);
            Color black = Color.FromArgb(0, 0, 0);
            Color white = Color.FromArgb(255, 255, 255);

            for (int x = 0; x < newBitmap.Width; x++)
            {
                for (int y = 0; y < newBitmap.Height; y++)
                {
                    int valueR = original.GetPixel(x, y).R;  //取得原始圖片的像素灰階值

                    Color newColor = valueR > grayscale ? white : black;//重構顏色

                    newBitmap.SetPixel(x, y, newColor); //將轉換後的灰階存回新的圖片
                }
            }

            return newBitmap;
        }

    }
}
