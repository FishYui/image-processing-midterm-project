﻿namespace _108325106影像處理期中Project
{
    partial class Binarization
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.openFile = new System.Windows.Forms.Button();
            this.GrayscaleText = new System.Windows.Forms.TextBox();
            this.newPicture = new System.Windows.Forms.PictureBox();
            this.originalPicture = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.newPicture)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.originalPicture)).BeginInit();
            this.SuspendLayout();
            // 
            // trackBar1
            // 
            this.trackBar1.Location = new System.Drawing.Point(123, 12);
            this.trackBar1.Maximum = 255;
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Size = new System.Drawing.Size(200, 45);
            this.trackBar1.TabIndex = 0;
            this.trackBar1.ValueChanged += new System.EventHandler(this.trackBar1_ValueChanged);
            // 
            // openFile
            // 
            this.openFile.Location = new System.Drawing.Point(12, 12);
            this.openFile.Name = "openFile";
            this.openFile.Size = new System.Drawing.Size(75, 23);
            this.openFile.TabIndex = 2;
            this.openFile.Text = "選擇檔案";
            this.openFile.UseVisualStyleBackColor = true;
            this.openFile.Click += new System.EventHandler(this.openFile_Click);
            // 
            // GrayscaleText
            // 
            this.GrayscaleText.Location = new System.Drawing.Point(341, 14);
            this.GrayscaleText.MaxLength = 3;
            this.GrayscaleText.Name = "GrayscaleText";
            this.GrayscaleText.Size = new System.Drawing.Size(100, 22);
            this.GrayscaleText.TabIndex = 3;
            this.GrayscaleText.TextChanged += new System.EventHandler(this.GrayscaleText_TextChanged);
            // 
            // newPicture
            // 
            this.newPicture.Location = new System.Drawing.Point(489, 120);
            this.newPicture.Name = "newPicture";
            this.newPicture.Size = new System.Drawing.Size(240, 210);
            this.newPicture.TabIndex = 5;
            this.newPicture.TabStop = false;
            // 
            // originalPicture
            // 
            this.originalPicture.Location = new System.Drawing.Point(71, 120);
            this.originalPicture.Name = "originalPicture";
            this.originalPicture.Size = new System.Drawing.Size(240, 210);
            this.originalPicture.TabIndex = 4;
            this.originalPicture.TabStop = false;
            // 
            // Binarization
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.newPicture);
            this.Controls.Add(this.originalPicture);
            this.Controls.Add(this.GrayscaleText);
            this.Controls.Add(this.openFile);
            this.Controls.Add(this.trackBar1);
            this.Name = "Binarization";
            this.Text = "Binarization";
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.newPicture)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.originalPicture)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TrackBar trackBar1;
        private System.Windows.Forms.Button openFile;
        private System.Windows.Forms.TextBox GrayscaleText;
        private System.Windows.Forms.PictureBox newPicture;
        private System.Windows.Forms.PictureBox originalPicture;
    }
}