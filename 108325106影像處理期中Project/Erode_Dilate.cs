﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _108325106影像處理期中Project
{
    public partial class Erode_Dilate : Form
    {

        public const int Mask = 5;  //3*3 => 1*2+1 、 15*15 => 7*2+1
        public Color black = Color.FromArgb(0, 0, 0);
        public Color white = Color.FromArgb(255, 255, 255);

        public Erode_Dilate()
        {
            InitializeComponent();
        }

        private void openFile_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog dlg = new OpenFileDialog())
            {
                dlg.Title = "Open Image";
                dlg.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png";

                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    originalPicture.Image = new Bitmap(dlg.FileName);
                    originalPicture.SizeMode = PictureBoxSizeMode.StretchImage;
                }
            }
        }

        private void erode_btn_Click(object sender, EventArgs e)
        {
            if (originalPicture.Image != null)
            {
                Bitmap _originalPicture = new Bitmap(originalPicture.Image);

                erodePicture.Image = Erode(_originalPicture);
                erodePicture.SizeMode = PictureBoxSizeMode.StretchImage;
            }
        }

        public Bitmap Erode(Bitmap original)
        {
            Bitmap newBitmap = new Bitmap(original.Width, original.Height);

            for (int x = 0; x < original.Width; x++)
            {
                for (int y = 0; y < original.Height; y++)
                {
                    bool result = CheckErode(original, x, y);  //取得原圖像素

                    if (result) //以此像素為原點，遮罩範圍內全都是白色時設為白
                        newBitmap.SetPixel(x, y, white);
                    else  //以此像素為原點，遮罩範圍內有黑色時設為黑
                        newBitmap.SetPixel(x, y, black);
                }
            }

            return newBitmap;
        }

        public bool CheckErode(Bitmap original, int X, int Y)
        {
            Color originalColor;

            for (int i = -Mask; i < Mask + 1; i++)
            {
                for (int j = -Mask; j < Mask + 1; j++)
                {
                    if (X + i < 0 || X + i >= original.Width || Y + j < 0 || Y + j >= original.Height)  //防止index超出相片範圍
                        continue;

                    originalColor = original.GetPixel(X + i, Y + j);  //取得遮罩範圍內的像素 

                    if (originalColor.R == 0)  //判斷像素顏色是否為黑
                        return false;
                }
            }

            return true;
        }

        private void dilate_btn_Click(object sender, EventArgs e)
        {
            if (originalPicture.Image != null)
            {
                Bitmap _originalPicture = new Bitmap(originalPicture.Image);

                dilatePicture.Image = Dilate(_originalPicture);
                dilatePicture.SizeMode = PictureBoxSizeMode.StretchImage;
            }
        }

        public Bitmap Dilate(Bitmap original)
        {
            Bitmap newBitmap = new Bitmap(original.Width, original.Height);

            for (int x = 0; x < original.Width; x++)
            {
                for (int y = 0; y < original.Height; y++)
                {
                    bool result = CheckDilate(original, x, y);  //取得原圖像素

                    if (result)  //以此像素為原點，遮罩範圍有白色時設為白
                        newBitmap.SetPixel(x, y, white);
                    else  //以此像素為原點，遮罩範圍都沒白色時設為黑
                        newBitmap.SetPixel(x, y, black);
                }
            }

            return newBitmap;
        }

        public bool CheckDilate(Bitmap original, int X, int Y)
        {
            Color originalColor;

            for (int i = -Mask; i < Mask + 1; i++)
            {
                for (int j = -Mask; j < Mask + 1; j++)
                {
                    if (X + i < 0 || X + i >= original.Width || Y + j < 0 || Y + j >= original.Height)  //防止index超出相片範圍
                        continue;

                    originalColor = original.GetPixel(X + i, Y + j);  //取得遮罩範圍內的像素 

                    if (originalColor.R != 255)  //判斷像素顏色是否為白
                        continue;

                    if (i != 0 && j != 0)  //判斷像素是否為自己
                        return true;
                }
            }

            return false;
        }

    }
}
