﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _108325106影像處理期中Project
{
    public partial class Sobel : Form
    {
        public Sobel()
        {
            InitializeComponent();
        }

        private void openFile_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog dlg = new OpenFileDialog())
            {
                dlg.Title = "Open Image";
                dlg.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png";

                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    originalPicture.Image = new Bitmap(dlg.FileName);
                    originalPicture.SizeMode = PictureBoxSizeMode.StretchImage;
                }
            }
        }

        private void convert_Click(object sender, EventArgs e)
        {
            if (originalPicture.Image != null)
            {
                Bitmap _originalPicture = new Bitmap(originalPicture.Image);

                newPicture.Image = MakeSobel(_originalPicture);
                newPicture.SizeMode = PictureBoxSizeMode.StretchImage;
            }
        }

        public Bitmap MakeSobel(Bitmap original)
        {
            Bitmap newBitmap = new Bitmap(original.Width, original.Height);

            for (int x = 1; x < original.Width - 1; x++)
            {
                for (int y = 1; y < original.Height - 1; y++)
                {
                    newBitmap.SetPixel(x, y, SobelMask33(original, x, y));
                }
            }

            return newBitmap;
        }

        public Color SobelMask33(Bitmap original, int X, int Y)
        {
            int[] maskx = { -1, 0, 1, -2, 0, 2, -1, 0, 1 };
            int[] masky = { 1, 2, 1, 0, 0, 0, -1, -2, -1 };

            int Index = 0;
            int Gx = 0, Gy = 0;

            for (int x = -1; x < 2; x++)
            {
                for (int y = -1; y < 2; y++)
                {
                    Gx += (original.GetPixel(X + x, Y + y).R * maskx[Index]);
                    Gy += (original.GetPixel(X + x, Y + y).R * masky[Index]);

                    Index++;
                }
            }

            int value = (int)Math.Pow((Gx * Gx + Gy * Gy), 0.5);

            value = Math.Abs(255 - (value > 255 ? 255 : value));

            return Color.FromArgb(value, value, value);
        }
    }
}
