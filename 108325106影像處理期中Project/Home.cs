﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _108325106影像處理期中Project
{
    public partial class Home : Form
    {
        public Home()
        {
            InitializeComponent();
        }

        private void RGB_To_Grey_Btn_Click(object sender, EventArgs e)
        {
            RGB_To_Grey f = new RGB_To_Grey();
            f.Show(this);
        }

        private void channel_conver_Click(object sender, EventArgs e)
        {
            Channel_Conver f = new Channel_Conver();
            f.Show(this);
        }

        private void Binarization_Click(object sender, EventArgs e)
        {
            Binarization f = new Binarization();
            f.Show(this);
        }

        private void Erode_Dilate_Click(object sender, EventArgs e)
        {
            Erode_Dilate f = new Erode_Dilate();
            f.Show(this);
        }

        private void Denoise_Click(object sender, EventArgs e)
        {
            Denoise f = new Denoise();
            f.Show(this);
        }

        private void Sharpen_Click(object sender, EventArgs e)
        {
            Sharpen f = new Sharpen();
            f.Show(this);
        }

        private void Sobel_Click(object sender, EventArgs e)
        {
            Sobel f = new Sobel();
            f.Show(this);
        }

        private void Histogram_Click(object sender, EventArgs e)
        {
            Histogram f = new Histogram();
            f.Show(this);
        }

        private void Rotation_Click(object sender, EventArgs e)
        {
            Rotation f = new Rotation();
            f.Show(this);
        }
    }
}
