﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _108325106影像處理期中Project
{
    public partial class Channel_Conver : Form
    {
        public Channel_Conver()
        {
            InitializeComponent();
        }

        private void openFile_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog dlg = new OpenFileDialog())
            {
                dlg.Title = "Open Image";
                dlg.Filter = "Image files (*.jpg, *.jpeg, *.jpe, *.jfif, *.png) | *.jpg; *.jpeg; *.jpe; *.jfif; *.png";

                if (dlg.ShowDialog() == DialogResult.OK)
                {
                    originalPicture.Image = new Bitmap(dlg.FileName);
                    originalPicture.SizeMode = PictureBoxSizeMode.StretchImage;
                }
            }
        }

        private void convert_Click(object sender, EventArgs e)
        {
            if (originalPicture.Image != null)
            {
                Bitmap _originalPicture = new Bitmap(originalPicture.Image);
                converRGB result = conver(_originalPicture);

                pictureR.Image = result.R;
                pictureR.SizeMode = PictureBoxSizeMode.StretchImage;

                pictureG.Image = result.G;
                pictureG.SizeMode = PictureBoxSizeMode.StretchImage;

                pictureB.Image = result.B;
                pictureB.SizeMode = PictureBoxSizeMode.StretchImage;
            }
        }

        public converRGB conver(Bitmap original)
        {
            converRGB newBitmap = new converRGB();

            newBitmap.R = new Bitmap(original.Width, original.Height);
            newBitmap.G = new Bitmap(original.Width, original.Height);
            newBitmap.B = new Bitmap(original.Width, original.Height);


            for (int x = 0; x < original.Width; x++)
            {
                for (int y = 0; y < original.Height; y++)
                {
                    Color originalColor = original.GetPixel(x, y);

                    Color R = Color.FromArgb(originalColor.R, originalColor.R, originalColor.R);
                    newBitmap.R.SetPixel(x, y, R);

                    Color G = Color.FromArgb(originalColor.G, originalColor.G, originalColor.G);
                    newBitmap.G.SetPixel(x, y, G);

                    Color B = Color.FromArgb(originalColor.B, originalColor.B, originalColor.B);
                    newBitmap.B.SetPixel(x, y, B);
                }
            }

            return newBitmap;
        }

        public class converRGB
        {
            public Bitmap R { get; set; }
            public Bitmap G { get; set; }
            public Bitmap B { get; set; }
        }
    }
}
